﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;


        public Deck()
        {
        }

        public Card this[ItalianSeed seed, ItalianValue value]
        {
            get
            {
                if(GetCardIndex(seed, value) == -1)
                {
                    return null;
                }
                return cards[GetCardIndex(seed, value)];
            }
            
        }

        private int GetCardIndex(ItalianSeed seed, ItalianValue value)
        {
            int i;
            for (i = 0; i < 40; i++)
            {
                Console.WriteLine("Seed passed: {0}   Seed current {1}", seed, cards[i].Seed);
               // Console.WriteLine("Val passed: {0}   Val current {1}\n", value, cards[i].Value);
                if (cards[i].Seed.Equals(seed) && cards[i].Value.Equals(value))
                {
                    return i;
                }
            }
            return -1;
        }
        public void Initialize()
        {

            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */

            int i = 0;
            this.cards = new Card[40];

        foreach (string seed in Enum.GetNames(typeof(ItalianSeed)))
            {
                foreach(var value in Enum.GetNames(typeof(ItalianValue)))
                {
                    Card card = new Card()
                    {
                        Seed = seed,
                        Value = value
                    };

                    this.cards[i] = card;
                    i++;
           
                }
            }

            //throw new NotImplementedException();
        }

        public void Print()
        {

            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
       
            foreach(Card card in cards)
            {
                Console.WriteLine("Card: {0}", card);
            }

            //throw new NotImplementedException();
        }
    }

    enum ItalianSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
